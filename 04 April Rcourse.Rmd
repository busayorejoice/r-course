---
title: "R Notebook"
output: html_notebook
---

This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook. When you execute code within the notebook, the results appear beneath the code. 

Try executing this chunk by clicking the *Run* button within the chunk or by placing your cursor inside it and pressing *Ctrl+Shift+Enter*. 

```{r}
f <- function(x)
print(x)
```

```{r}
g <- function(x,y){
  x+y^2}
```

```{r}
g(10,2)
g(100,10)
my.result <- g(100,10)
g(x=10, y=2)
g(y = a, x = 2)
b <- g(y = a, x = 2)
```

```{r}
funcs <- list(f,g)
funcs
```

```{r}
hh <- function(x=0, y=0){
  c(x + y^2, x + y^3)
  
}
```

```{r}
ff <- function(x=0, f){
  f(x)
}
```

```{r}
hh(y=20)
hh(y=20)[2:1]
hh(y=20)[c(2, 2, 1)]

```

```{r}
pp <-function(x =0, y = 0) {
  if(x < 1) x<- -x else x <- x^2
  c(x+ y^2, x+y^3)
}
```

```{r}
pp(-1, 5)
pp(1, 5)
```

```{r}
funcs <- list(mean, var, sum)
numbers <- rnorm(20)
```

```{r}
for (f in funcs) {
  print(f(numbers))
}
```

```{r}
for (i in seq(along.with = list())) {
  print(list()[[i]](numbers))
}
```

```{r}
result.vec <- numeric(0)
for (f in funcs) {
  result.vec <-c(result.vec, f(numbers))
}
result.vec
```

```{r}
x <- 100
result <- numeric()
while(x > 2) { 
  x <- x/2
  result <-c(result, x)
  if ((x >2)) break()
}
result
```

```{r}
two.col.matrix <- matrix(1:6, ncol = 2)
two.col.matrix
two.col.matrix[2,]
```

```{r}
set.seed(123456)
a.vector <- runif(10)
my.fun <- function(x, k) {log(x) + k}
z <- lapply(X = a.vector, FUN = my.fun, k = 5)
class(z)
z
```

```{r}
my.tb <- tibble(numbers = 1:9, letters = rep(letters[1:3], 3))
my_gr.tb <- group_by(my.tb, letters)
my_gr.tb
```

```{r}
123 %>% print()
123 %>% print() %>% print()
```

```{r}
1:100 %>% mean()
```

```{r}

```

```{r}
ff(1:10, mean)
```

Add a new chunk by clicking the *Insert Chunk* button on the toolbar or by pressing *Ctrl+Alt+I*.

When you save the notebook, an HTML file containing the code and output will be saved alongside it (click the *Preview* button or press *Ctrl+Shift+K* to preview the HTML file).
